CREATE TABLE AERONAUTICA (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
tipo_avion VARCHAR(30) NOT NULL,
identificacion_avion VARCHAR(30) NOT NULL,
nombre_piloto VARCHAR(30),
nombre_copiloto VARCHAR(30),
numero_pasajeros INT(6),
origen VARCHAR(30),
destino VARCHAR(30),
)