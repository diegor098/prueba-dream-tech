import { Component } from "@angular/core";
import { DataService } from "./data.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  data: any = [];

  displayDialog: boolean;

  row: any = {};

  selectedRow: any;

  newRow: boolean;

  cols: any[];

  constructor(protected dataService: DataService) {}

  ngOnInit() {
    this.getData();
    this.cols = [
      { field: "email", header: "Email" },
      { field: "gender", header: "Gender" },
      { field: "phone", header: "Phone" },
    ];
  }

  getData() {
    this.dataService.getData().subscribe(
      (data) => {
        this.data = data["results"];
      },
      (error) => {
        console.error(error);
      }
    );
  }

  showDialogToAdd() {
    this.newRow = true;
    this.row = {};
    this.displayDialog = true;
  }

  save() {
    let data = [...this.data];
    if (this.newRow) data.push(this.row);
    else data[this.data.indexOf(this.selectedRow)] = this.row;

    this.data = data;
    this.row = null;
    this.displayDialog = false;
  }

  delete() {
    let index = this.data.indexOf(this.selectedRow);
    this.data = this.data.filter((val, i) => i != index);
    this.row = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.newRow = false;
    this.row = this.cloneCar(event.data);
    this.displayDialog = true;
  }

  cloneCar(r: any): any {
    let row = {};
    for (let prop in r) {
      row[prop] = r[prop];
    }
    return row;
  }
}
